<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230521151843 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE messenger_messages (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, body CLOB NOT NULL, headers CLOB NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at DATETIME NOT NULL, available_at DATETIME NOT NULL, delivered_at DATETIME DEFAULT NULL)');
        $this->addSql('CREATE INDEX IDX_75EA56E0FB7336F0 ON messenger_messages (queue_name)');
        $this->addSql('CREATE INDEX IDX_75EA56E0E3BD61CE ON messenger_messages (available_at)');
        $this->addSql('CREATE INDEX IDX_75EA56E016BA31DB ON messenger_messages (delivered_at)');
        $this->addSql('DROP TABLE Characters');
        $this->addSql('DROP TABLE Media');
        $this->addSql('DROP TABLE MediaTypes');
        $this->addSql('DROP TABLE VoiceActors');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE Characters (CharacterID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, Name CLOB DEFAULT NULL COLLATE "BINARY", PhotoPath CLOB DEFAULT NULL COLLATE "BINARY", VoicedBy INTEGER DEFAULT NULL, AppearsIn INTEGER DEFAULT NULL, FOREIGN KEY (AppearsIn) REFERENCES Media (MediaID) ON UPDATE NO ACTION ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE, FOREIGN KEY (VoicedBy) REFERENCES VoiceActors (VoiceActorID) ON UPDATE NO ACTION ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('CREATE INDEX IDX_757442DE8F838448 ON Characters (AppearsIn)');
        $this->addSql('CREATE INDEX IDX_757442DED24CCAF9 ON Characters (VoicedBy)');
        $this->addSql('CREATE TABLE Media (MediaID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, Name CLOB DEFAULT NULL COLLATE "BINARY", ImagePath CLOB DEFAULT NULL COLLATE "BINARY", Type INTEGER NOT NULL, FOREIGN KEY (Type) REFERENCES MediaTypes (MediaTypeID) ON UPDATE NO ACTION ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('CREATE INDEX IDX_ABED8E082CECF817 ON Media (Type)');
        $this->addSql('CREATE TABLE MediaTypes (MediaTypeID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, Name CLOB DEFAULT NULL COLLATE "BINARY")');
        $this->addSql('CREATE TABLE VoiceActors (VoiceActorID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, FirstName CLOB DEFAULT NULL COLLATE "BINARY", LastName CLOB DEFAULT NULL COLLATE "BINARY", PhotoPath CLOB DEFAULT NULL COLLATE "BINARY")');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
