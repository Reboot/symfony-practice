<?php

namespace App\Controller;

use App\Entity\MediaType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MediaTypeController extends AbstractController
{
    #[Route('/type/{id}', 'type')]
    public function show(MediaType $type): Response
    {
        return $this->render('mediatype.html.twig', [
            'type' => $type
        ]);
    }
}