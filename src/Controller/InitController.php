<?php

namespace App\Controller;

use App\Entity\MediaType;
use App\Entity\VoiceActor;
use App\Entity\Media;
use App\Entity\Character;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Routing\Annotation\Route;

class InitController extends AbstractController
{
    #[Route('/init/all')]
    public function page(EntityManagerInterface $entityManager): Response
    {
        
        $vn = new MediaType();
        $vn->setName('Visual novel');

        $game = new MediaType();
        $game->setName('Game');

        $anime = new MediaType();
        $anime->setName('Anime');

        // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $entityManager->persist($vn);
        $entityManager->persist($game);
        $entityManager->persist($anime);

        $va = new VoiceActor();
        $va->setFirstName("Daisuke");
        $va->setLastName("Ono");
        $entityManager->persist($va);

        $vn_type = $entityManager->getRepository(MediaType::class)->findOneBy(
            ['name' => 'Visual novel']
        );
        
        $umineko = new Media();
        $umineko->setName("Umineko no naku koro ni");
        $umineko->setType($vn_type);
        $entityManager->persist($umineko);

        $battler = new Character();
        $battler->setName("Ushiromiya Battler");
        $battler->setAppearsIn($umineko);
        $battler->setVoicedBy($va);
        $entityManager->persist($battler);

        // actually executes the queries (i.e. the INSERT query)
        $entityManager->flush();
        
        return new Response("okkei!");
    }
}