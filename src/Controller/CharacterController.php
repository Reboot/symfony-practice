<?php

namespace App\Controller;

use App\Entity\Character;
use App\Form\CharacterType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\SubmitButton;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/character')]
class CharacterController extends AbstractController
{
    #[Route('', 'character_list')]
    public function show(EntityManagerInterface $entityManager): Response
    {
        $characters = $entityManager->getRepository(Character::class)->findAll();
        return $this->render('characters.html.twig', [
            'characters' => $characters
        ]);
    }

    #[Route('/id/{id}', 'character_id')]
    public function show_id(Character $character): Response
    {
        return $this->render('character.html.twig', [
            'character' => $character
        ]);
    }

    #[Route('/new', name: 'character_new', methods: ['GET', 'POST'])]
    public function new(
        Request $request,
        EntityManagerInterface $entityManager,
    ): Response {
        $char = new Character();

        // See https://symfony.com/doc/current/form/multiple_buttons.html
        $form = $this->createForm(CharacterType::class, $char);

        $form->handleRequest($request);

        // the isSubmitted() method is completely optional because the other
        // isValid() method already checks whether the form is submitted.
        // However, we explicitly add it to improve code readability.
        // See https://symfony.com/doc/current/forms.html#processing-forms
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($char);
            $entityManager->flush();

            // Flash messages are used to notify the user about the result of the
            // actions. They are deleted automatically from the session as soon
            // as they are accessed.
            // See https://symfony.com/doc/current/controller.html#flash-messages
            $this->addFlash('success', 'post.created_successfully');

            return $this->redirectToRoute('character_list');
        }

        return $this->render('character_new.html.twig', [
            'char' => $char,
            'form' => $form,
            'verb' => 'Add',
        ]);
    }

    #[Route('/edit/{id}', name: 'character_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Character $char, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(CharacterType::class, $char);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();
            $this->addFlash('success', 'post.updated_successfully');

            return $this->redirectToRoute('character_id', ['id' => $char->getId()]);
        }

        return $this->render('character_new.html.twig', [
            'char' => $char,
            'form' => $form,
            'verb' => 'Edit'
        ]);
    }
}