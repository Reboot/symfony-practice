<?php

namespace App\Controller;

use App\Entity\VoiceActor;
use App\Form\VoiceActorType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\SubmitButton;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/va')]
class VoiceActorController extends AbstractController
{
    #[Route('', 'va_list')]
    public function show(EntityManagerInterface $entityManager): Response
    {
        $vas = $entityManager->getRepository(VoiceActor::class)->findAll();
        return $this->render('voiceactors.html.twig', [
            'vas' => $vas
        ]);
    }

    #[Route('/id/{id}', 'va_id')]
    public function show_id(VoiceActor $va): Response
    {
        return $this->render('voiceactor.html.twig', [
            'va' => $va
        ]);
    }

    #[Route('/new', name: 'va_new', methods: ['GET', 'POST'])]
    public function new(
        Request $request,
        EntityManagerInterface $entityManager,
    ): Response {
        $va = new VoiceActor();

        // See https://symfony.com/doc/current/form/multiple_buttons.html
        $form = $this->createForm(VoiceActorType::class, $va);

        $form->handleRequest($request);

        // the isSubmitted() method is completely optional because the other
        // isValid() method already checks whether the form is submitted.
        // However, we explicitly add it to improve code readability.
        // See https://symfony.com/doc/current/forms.html#processing-forms
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($va);
            $entityManager->flush();

            // Flash messages are used to notify the user about the result of the
            // actions. They are deleted automatically from the session as soon
            // as they are accessed.
            // See https://symfony.com/doc/current/controller.html#flash-messages
            $this->addFlash('success', 'post.created_successfully');

            return $this->redirectToRoute('va_list');
        }

        return $this->render('voiceactor_new.html.twig', [
            'va' => $va,
            'form' => $form,
            'verb' => 'Add'
        ]);
    }

    #[Route('/edit/{id}', name: 'va_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, VoiceActor $va, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(VoiceActorType::class, $va);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();
            $this->addFlash('success', 'post.updated_successfully');

            return $this->redirectToRoute('va_id', ['id' => $va->getId()]);
        }

        return $this->render('voiceactor_new.html.twig', [
            'va' => $va,
            'form' => $form,
            'verb' => 'Edit'
        ]);
    }
}