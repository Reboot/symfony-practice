<?php

namespace App\Controller;

use App\Entity\MediaType;
use App\Entity\Media;
use App\Entity\Character;
use App\Entity\VoiceActor;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('', 'home')]
    public function show(EntityManagerInterface $entityManager): Response
    {
        $types = $entityManager->getRepository(MediaType::class)->findAll();
        $medias = $entityManager->getRepository(Media::class)->findAll();
        $characters = $entityManager->getRepository(Character::class)->findAll();
        $vas = $entityManager->getRepository(VoiceActor::class)->findAll();
    
        return $this->render('home.html.twig', [
            'types' => $types, 'medias' => $medias, 'characters' => $characters, 'vas' => $vas
        ]);
    }
}