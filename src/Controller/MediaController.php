<?php

namespace App\Controller;

use App\Entity\Media;
use App\Form\MediaFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\SubmitButton;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/media')]
class MediaController extends AbstractController
{
    #[Route('', 'media_list')]
    public function show(EntityManagerInterface $entityManager): Response
    {
        $medias = $entityManager->getRepository(Media::class)->findAll();
        return $this->render('medias.html.twig', [
            'medias' => $medias
        ]);
    }

    #[Route('/id/{id}', 'media_id')]
    public function show_id(Media $media): Response
    {
        return $this->render('media.html.twig', [
            'media' => $media
        ]);
    }

    #[Route('/new', name: 'media_new', methods: ['GET', 'POST'])]
    public function new(
        Request $request,
        EntityManagerInterface $entityManager,
    ): Response {
        $media = new Media();

        // See https://symfony.com/doc/current/form/multiple_buttons.html
        $form = $this->createForm(MediaFormType::class, $media);

        $form->handleRequest($request);

        // the isSubmitted() method is completely optional because the other
        // isValid() method already checks whether the form is submitted.
        // However, we explicitly add it to improve code readability.
        // See https://symfony.com/doc/current/forms.html#processing-forms
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($media);
            $entityManager->flush();

            // Flash messages are used to notify the user about the result of the
            // actions. They are deleted automatically from the session as soon
            // as they are accessed.
            // See https://symfony.com/doc/current/controller.html#flash-messages
            $this->addFlash('success', 'post.created_successfully');

            return $this->redirectToRoute('media_list');
        }

        return $this->render('media_new.html.twig', [
            'media' => $media,
            'form' => $form,
            'verb' => 'Add'
        ]);
    }

    #[Route('/edit/{id}', name: 'media_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Media $media, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(CharacterType::class, $media);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();
            $this->addFlash('success', 'post.updated_successfully');

            return $this->redirectToRoute('character_id', ['id' => $media->getId()]);
        }

        return $this->render('media_new.html.twig', [
            'media' => $media,
            'form' => $form,
            'verb' => 'Edit'
        ]);
    }
}