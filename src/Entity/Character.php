<?php

namespace App\Entity;

use App\Repository\CharacterRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CharacterRepository::class)]
class Character
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\ManyToOne(inversedBy: 'characters')]
    #[ORM\JoinColumn(nullable: false)]
    private ?VoiceActor $voiced_by = null;

    #[ORM\ManyToOne(inversedBy: 'characters')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Media $appears_in = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getVoicedBy(): ?VoiceActor
    {
        return $this->voiced_by;
    }

    public function setVoicedBy(?VoiceActor $voiced_by): self
    {
        $this->voiced_by = $voiced_by;

        return $this;
    }

    public function getAppearsIn(): ?Media
    {
        return $this->appears_in;
    }

    public function setAppearsIn(?Media $appears_in): self
    {
        $this->appears_in = $appears_in;

        return $this;
    }
}
