# Symfony Practice

## Instructions

1. `$ git clone https://codeberg.org/Reboot/symfony-practice.git`
2. `$ symfony run composer install`
3. `$ symfony run npm install`
4. `$ symfony run npm run dev`
5. `$ symfony server:start`